# Magento2-Demo: Backend Menu

Wrapper for vendor modules config.

##

```bash
$ composer config repositories.demo.backend-menu vcs git@gitlab.com:sreichel/magento2-demo-backend-menu-magento2.git
```
---
```bash
$ composer require mse-sv3n/magento2-demo-module-backend-menu
```
